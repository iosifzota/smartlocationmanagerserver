package com.slm.slm1.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the task database table.
 * 
 */
@Entity
@NamedQuery(name="Task.findAll", query="SELECT t FROM Task t")
public class Task implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="completion_status")
	private int completionStatus;

	private String name;
	
	@Column(name = "local_task_id")
	private int localTaskId;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "deadline")
	private Date deadline;

	//bi-directional many-to-one association to Location
	@ManyToOne
	@JoinColumn(name="location_fk")
	private Location location;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_fk")
	private User user;

	//bi-directional one-to-one association to TaskDetail
	@OneToOne(mappedBy="task")
	private TaskDetail taskDetail;

	//bi-directional one-to-one association to ReceivedTask
	@OneToOne(mappedBy="task")
	private ReceivedTask receivedTask;

	public Task() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCompletionStatus() {
		return this.completionStatus;
	}

	public void setCompletionStatus(int completionStatus) {
		this.completionStatus = completionStatus;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public TaskDetail getTaskDetail() {
		return this.taskDetail;
	}

	public void setTaskDetail(TaskDetail taskDetail) {
		this.taskDetail = taskDetail;
	}

	public ReceivedTask getReceivedTask() {
		return this.receivedTask;
	}

	public void setReceivedTask(ReceivedTask receivedTask) {
		this.receivedTask = receivedTask;
	}

	public int getLocalTaskId() {
		return localTaskId;
	}

	public void setLocalTaskId(int localTaskId) {
		this.localTaskId = localTaskId;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	
}