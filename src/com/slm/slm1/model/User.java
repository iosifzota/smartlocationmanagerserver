package com.slm.slm1.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private int userId;

	private String password;

	@Column(name="user_details_fk")
	private int userDetailsFk;

	private String username;

	//bi-directional many-to-one association to Task
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Task> tasks;

	//bi-directional many-to-many association to User
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
		name="friendship"
		, joinColumns={
			@JoinColumn(name="main_user_fk")
			}
		, inverseJoinColumns={
			@JoinColumn(name="friend_fk")
			}
		)
	private List<User> users1;

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="users1", fetch=FetchType.EAGER)
	private List<User> users2;

	//bi-directional many-to-one association to ReceivedTask
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<ReceivedTask> receivedTasks;

	public User() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserDetailsFk() {
		return this.userDetailsFk;
	}

	public void setUserDetailsFk(int userDetailsFk) {
		this.userDetailsFk = userDetailsFk;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Task addTask(Task task) {
		getTasks().add(task);
		task.setUser(this);

		return task;
	}

	public Task removeTask(Task task) {
		getTasks().remove(task);
		task.setUser(null);

		return task;
	}

	public List<User> getUsers1() {
		return this.users1;
	}

	public void setUsers1(List<User> users1) {
		this.users1 = users1;
	}

	public List<User> getUsers2() {
		return this.users2;
	}

	public void setUsers2(List<User> users2) {
		this.users2 = users2;
	}

	public List<ReceivedTask> getReceivedTasks() {
		return this.receivedTasks;
	}

	public void setReceivedTasks(List<ReceivedTask> receivedTasks) {
		this.receivedTasks = receivedTasks;
	}

	public ReceivedTask addReceivedTask(ReceivedTask receivedTask) {
		getReceivedTasks().add(receivedTask);
		receivedTask.setUser(this);

		return receivedTask;
	}

	public ReceivedTask removeReceivedTask(ReceivedTask receivedTask) {
		getReceivedTasks().remove(receivedTask);
		receivedTask.setUser(null);

		return receivedTask;
	}

}