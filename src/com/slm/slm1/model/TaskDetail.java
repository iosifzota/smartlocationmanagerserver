package com.slm.slm1.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the task_details database table.
 * 
 */
@Entity
@Table(name="task_details")
@NamedQuery(name="TaskDetail.findAll", query="SELECT t FROM TaskDetail t")
public class TaskDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", insertable=false, updatable=false)
	private int id;

	private String notes;

	//bi-directional one-to-one association to Task
	@OneToOne
	@JoinColumn(name="id")
	private Task task;

	public TaskDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Task getTask() {
		return this.task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

}