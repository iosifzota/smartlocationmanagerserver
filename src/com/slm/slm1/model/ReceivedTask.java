package com.slm.slm1.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the received_task database table.
 * 
 */
@Entity
@Table(name="received_task")
@NamedQuery(name="ReceivedTask.findAll", query="SELECT r FROM ReceivedTask r")
public class ReceivedTask implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="task_id", insertable=false, updatable=false)
	private int taskId;

	//bi-directional one-to-one association to Task
	@OneToOne
	@JoinColumn(name = "task_id")
	private Task task;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	public ReceivedTask() {
	}

	public int getTaskId() {
		return this.taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public Task getTask() {
		return this.task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}