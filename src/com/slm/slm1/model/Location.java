package com.slm.slm1.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private double latitude;

	@Column(name="location_details_fk")
	private int locationDetailsFk;

	private double longitude;

	private String name;

	//bi-directional many-to-one association to Task
	@OneToMany(mappedBy="location", fetch=FetchType.EAGER)
	private List<Task> tasks;

	public Location() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getLocationDetailsFk() {
		return this.locationDetailsFk;
	}

	public void setLocationDetailsFk(int locationDetailsFk) {
		this.locationDetailsFk = locationDetailsFk;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Task addTask(Task task) {
		getTasks().add(task);
		task.setLocation(this);

		return task;
	}

	public Task removeTask(Task task) {
		getTasks().remove(task);
		task.setLocation(null);

		return task;
	}

}