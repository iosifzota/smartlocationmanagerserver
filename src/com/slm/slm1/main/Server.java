package com.slm.slm1.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.helper.AuthRunnerCreator;
import com.slm.slm1.helper.DeleteTasksRunnerCreator;
import com.slm.slm1.helper.FriendsListRunnerCreator;
import com.slm.slm1.helper.AddFriendRunnerCreator;
import com.slm.slm1.helper.IncomingRunnerCreator;
import com.slm.slm1.helper.OutgoingRunnerCreator;
import com.slm.slm1.helper.RegisterRunnerCreator;
import com.slm.slm1.helper.RestoreTasksRunnerCreator;
import com.slm.slm1.helper.SaveTaskRunnerCreator;
import com.slm.slm1.helper.ServerDatabase;
import com.slm.slm1.helper.ServerRunner;
import com.slm.slm1.helper.ServerRunnerWithSync;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.helper.UpdateTasksRunnerCreator;
import com.slm.slm1.helper.UserConnection;
import com.slm.slm1.helper.UserConnectionRunner;
import com.slm.slm1.helper.UserCredentials;
import com.slm.slm1.helper.UsersRouter;
import com.slm.slm1.model.User;

public class Server {
	


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        
    	ReentrantLock lock = new ReentrantLock();
    	
        new Thread(new ServerRunnerWithSync(3001, new IncomingRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3002, new OutgoingRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3006, new SaveTaskRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3007, new RestoreTasksRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3008, new DeleteTasksRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3009, new UpdateTasksRunnerCreator(), lock)).start();
    }

    public static String toStringTask(TaskSerializable taskSerializable) {
        return taskSerializable.localTaskId + taskSerializable.taskName + taskSerializable.taskNotes + taskSerializable.locationLat + taskSerializable.locationLat + taskSerializable.targetUsername;
    }
}
