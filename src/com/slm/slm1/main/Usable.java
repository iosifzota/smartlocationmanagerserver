package com.slm.slm1.main;

import com.slm.slm1.helper.AddFriendRunnerCreator;
import com.slm.slm1.helper.AuthRunnerCreator;
import com.slm.slm1.helper.FriendsListRunnerCreator;
import com.slm.slm1.helper.RegisterRunnerCreator;
import com.slm.slm1.helper.ServerRunner;

public class Usable {

	public static void main(String[] args) {
		new Thread(new ServerRunner(3000, new AuthRunnerCreator())).start();
		new Thread(new ServerRunner(3003, new AddFriendRunnerCreator())).start();
		new Thread(new ServerRunner(3004, new RegisterRunnerCreator())).start();
        new Thread(new ServerRunner(3005, new FriendsListRunnerCreator())).start();
	}

}
