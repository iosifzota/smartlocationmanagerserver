package com.slm.slm1.main;

import java.util.concurrent.locks.ReentrantLock;

import com.slm.slm1.helper.AddFriendRunnerCreator;
import com.slm.slm1.helper.AuthRunnerCreator;
import com.slm.slm1.helper.DeleteTasksRunnerCreator;
import com.slm.slm1.helper.FriendsListRunnerCreator;
import com.slm.slm1.helper.IncomingRunnerCreator;
import com.slm.slm1.helper.OutgoingRunnerCreator;
import com.slm.slm1.helper.RegisterRunnerCreator;
import com.slm.slm1.helper.RestoreTasksRunnerCreator;
import com.slm.slm1.helper.SaveTaskRunnerCreator;
import com.slm.slm1.helper.ServerRunner;
import com.slm.slm1.helper.ServerRunnerWithSync;
import com.slm.slm1.helper.UpdatePasswordRunnerCreator;
import com.slm.slm1.helper.UpdateTasksRunnerCreator;

public class All {

	public static void main(String[] args) {
		
		new Thread(new ServerRunner(3000, new AuthRunnerCreator())).start();
		new Thread(new ServerRunner(3003, new AddFriendRunnerCreator())).start();
		new Thread(new ServerRunner(3004, new RegisterRunnerCreator())).start();
        new Thread(new ServerRunner(3005, new FriendsListRunnerCreator())).start();
		
		ReentrantLock lock = new ReentrantLock(); // used to sync database access ?
    	
        new Thread(new ServerRunnerWithSync(3001, new IncomingRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3002, new OutgoingRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3006, new SaveTaskRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3007, new RestoreTasksRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3008, new DeleteTasksRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3009, new UpdateTasksRunnerCreator(), lock)).start();
        new Thread(new ServerRunnerWithSync(3010, new UpdatePasswordRunnerCreator(), lock)).start();
	}

}
