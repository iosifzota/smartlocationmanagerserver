package com.slm.slm1.daoImpl;

import javax.persistence.EntityManager;

import com.slm.slm1.dao.IDao;
import com.slm.slm1.helper.DatabaseUtil;
import com.slm.slm1.helper.ServerDatabase;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.model.ReceivedTask;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetail;

public class TaskDao implements IDao<Task> {
	
	private EntityManager entityManager;
	
	public TaskDao(ServerDatabase serverDatabase) {
		entityManager = serverDatabase.getDatabaseConnection().getEntityManager();
	}

	@Override
	public void insert(Task entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();		
	}
	
	public void insert(TaskDetail taskDetail) {
		entityManager.getTransaction().begin();
		entityManager.persist(taskDetail);
		entityManager.getTransaction().commit();	
	}
	
	public void insert(ReceivedTask receivedTask) {
		entityManager.getTransaction().begin();
		entityManager.persist(receivedTask);
		entityManager.getTransaction().commit();	
	}

	@Override
	public Task get(int id) {
		return entityManager.find(Task.class, id);
	}
	
	public Task get(String taskName) {
		String queryString;
		queryString = "SELECT * FROM task WHERE name = \"" + taskName + "\"";
		return DatabaseUtil.getSingleResult(queryString, entityManager, Task.class);
	}
	
	public TaskDetail get(Task task) {
		return entityManager.find(TaskDetail.class, task.getId());
	}

	@Override
	public void update(Task entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}
	
	public void update(TaskDetail entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}
	
	public void delete(ReceivedTask received) {
		entityManager.getTransaction().begin();
		entityManager.remove(received);
		entityManager.getTransaction().commit();
	}

	@Override
	public void delete(Task entity) {
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.getTransaction().commit();
	}

	public Task get(String targetUsername, int localTaskId) {
		String queryString; 
		queryString = "SELECT * FROM task INNER JOIN user ON task.user_fk = user.user_id WHERE username = \"" + targetUsername + "\" AND local_task_id = " + localTaskId;
		return DatabaseUtil.getSingleResult(queryString, entityManager, Task.class);
	}

}
