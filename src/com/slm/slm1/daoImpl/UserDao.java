package com.slm.slm1.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.omg.CORBA.FREE_MEM;

import com.slm.slm1.dao.IDao;
import com.slm.slm1.helper.FriendSerializable;
import com.slm.slm1.helper.ServerDatabase;
import com.slm.slm1.model.User;

import sun.font.CreatedFontTracker;

public class UserDao implements IDao<User> {
	
	private EntityManager entityManager;
	
	public UserDao(ServerDatabase serverDatabase) {
		entityManager = serverDatabase.getDatabaseConnection().getEntityManager();
	}

	@Override
	public void insert(User entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}

	@Override
	public User get(int id) {
		
		return null;
	}
	
	public User get(String username) {
		User user = null;
		
		try {
			user = (User)entityManager.createNativeQuery("SELECT * FROM User WHERE username = \"" + username +"\" LIMIT 1", User.class).getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		} catch(NonUniqueResultException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public void update(User entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}

	@Override
	public void delete(User entity) {
		
	}
	
	public void insertFrendship(User senderUser, User targetUser) {
		senderUser = entityManager.find(User.class, senderUser.getUserId());
		
		entityManager.getTransaction().begin();
		senderUser.setUsers1(new ArrayList<>());
		senderUser.getUsers1().add(targetUser);
		entityManager.getTransaction().commit();
	}

	public List<User> getFriends(User user) {
		Query query = entityManager.createNativeQuery("SELECT friendship.username FROM friendship INNER JOIN user ON user.id = friendship.main_user_fk WHERE user.id = " + user.getUserId());
		
		List<User> friends = null;
		
		user = entityManager.find(User.class, user.getUserId());
		
		if (user != null) {
			friends = user.getUsers1();
		}
		
		if (friends == null) {
			friends = new ArrayList<>();
		}
		
		return friends;
	}

}
