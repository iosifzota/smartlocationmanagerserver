package com.slm.slm1.daoImpl;

import javax.persistence.EntityManager;

import com.slm.slm1.dao.IDao;
import com.slm.slm1.helper.DatabaseUtil;
import com.slm.slm1.helper.ServerDatabase;
import com.slm.slm1.model.Location;

public class LocationDao implements IDao<Location> {

	private EntityManager entityManager;
	
	public LocationDao(ServerDatabase serverDatabase) {
		entityManager = serverDatabase.getDatabaseConnection().getEntityManager();
	}
	
	@Override
	public void insert(Location entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}

	@Override
	public Location get(int id) {
		return entityManager.find(Location.class, id);
	}
	
	public Location get(double latitude, double longitude) {
		String queryString;
		queryString = "SELECT * FROM Location WHERE latitude = " + latitude
				+ " AND longitude = " + longitude; 
		
		Location location = DatabaseUtil.getSingleResult(queryString, entityManager, Location.class);
		return location;
	}

	@Override
	public void update(Location entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}

	@Override
	public void delete(Location entity) {
	}

}
