package com.slm.slm1.helper;

import java.io.Serializable;

public class UserCredentials implements Serializable {
    public String username;
    public String password;
}