package com.slm.slm1.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

public class DatabaseUtil {

	public static <Entity> Entity getSingleResult(String queryString, EntityManager entityManager, Class<Entity> cls) {
		Query query = entityManager.createNativeQuery(queryString, cls);
		
		Entity entity = null;
		List<Entity> result = null;
		
		try {
			result = query.getResultList();
		} catch (NoResultException e) {
			e.printStackTrace();
		} catch(NonUniqueResultException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (result != null && !result.isEmpty()) {
			entity = result.get(0);
		}
		
		return entity;
	}
}
