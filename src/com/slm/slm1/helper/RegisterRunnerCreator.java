package com.slm.slm1.helper;

import java.net.Socket;

public class RegisterRunnerCreator implements IConnectionRunnerCreator {

	@Override
	public ConnectionRunner newObject(Socket socket) {
		return new RegisterRunner(socket);
	}

}
