package com.slm.slm1.helper;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetail;
import com.slm.slm1.model.User;

public class UpdateTasksRunner extends ConnectionRunnerWithSync {

	public UpdateTasksRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {

		List<TaskSerializable> tasksToUpdate = (List<TaskSerializable>) getConn().readObject();
		
		if (tasksToUpdate != null && !tasksToUpdate.isEmpty()) {
			Boolean success = false;
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
			TaskDao taskDao = ServerDatabase.getInstance().getTaskDao();
			
			User user = userDao.get(tasksToUpdate.get(0).targetUsername);
			
			if (user != null) {
				
				for (TaskSerializable taskSerializable : tasksToUpdate) {
					Task task = taskDao.get(taskSerializable.localTaskId);
					
					if (task != null) {
						TaskDetail taskDetail = task.getTaskDetail();
						if (taskDetail == null) {
							taskDetail = taskDao.get(task);
						}
						
						task.setName(taskSerializable.taskName);
						task.setCompletionStatus(taskSerializable.completionStatus ? 1 : 0);
						task.setDeadline(taskSerializable.deadline);
						taskDao.update(task);
						
						taskDetail.setNotes(taskSerializable.taskNotes);
						taskDao.update(taskDetail);
						success = true;
					} else {
						success = false;
						break;
					}
				}
			}
			
			if (getConn().writeObject(success)) {
				// responded
			}
		}
		
		getConn().closeConnection();
		lock.unlock();
	}

}
