package com.slm.slm1.helper;

import java.net.Socket;

public interface IConnectionRunnerCreator {
	
	ConnectionRunner newObject(Socket socket);
}

