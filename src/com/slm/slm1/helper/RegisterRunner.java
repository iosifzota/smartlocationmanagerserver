package com.slm.slm1.helper;

import java.net.Socket;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.User;

public class RegisterRunner extends ConnectionRunner {

	public RegisterRunner(Socket socket) {
		super(socket);
	}

	@Override
	public void run() {
		System.out.println("Register runner");
		
		RegisterRequest registerRequest = (RegisterRequest) getConn().readObject();
		
		if (registerRequest != null) {
			RegisterResponse registerResponse = new RegisterResponse();
			registerResponse.success = false;
			
			// check if user is already registered
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
	        User user = userDao.get(registerRequest.userCredentials.username);
	        
	        if (user == null) {
	        	// insert new user
	        	user = new User();
	        	user.setUsername(registerRequest.userCredentials.username);
	        	user.setPassword(registerRequest.userCredentials.password);
	        	
	        	userDao.insert(user);
	        	
	        	// check if user was inserted
	        	User tmpUser = userDao.get(registerRequest.userCredentials.username);
	        	
	        	if (tmpUser != null) {
	        		registerResponse.success = true;
	        	}
	        }
	        
	        if(getConn().writeObject(registerResponse)) {
	        	// response send
	        }
		}
		
		getConn().closeConnection();
	}

}
