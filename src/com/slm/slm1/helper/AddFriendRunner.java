package com.slm.slm1.helper;

import java.net.Socket;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.User;

public class AddFriendRunner extends ConnectionRunner {

	
	public AddFriendRunner(Socket socket) {
		super(socket);
	}

	@Override
	public void run() {
		System.out.println("Friend runner");
		
		AddFriendRequest addFriendRequest = (AddFriendRequest) getConn().readObject();
		
		if (addFriendRequest != null) {
			AddFriendResponse addFriendResponse = new AddFriendResponse();
			addFriendResponse.success = false;
			
			// verify user names for both sender & target
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
			User senderUser = userDao.get(addFriendRequest.senderUsername);
			User targetUser = userDao.get(addFriendRequest.targetUsername);
			
			if (senderUser != null && targetUser != null) {
				// if ok add target as friend of sender
				userDao.insertFrendship(senderUser, targetUser);
				
				// check
				senderUser = userDao.get(addFriendRequest.senderUsername);
				
				if (senderUser.getUsers1() != null && senderUser.getUsers1().contains(targetUser)) {
					addFriendResponse.success = true;
				}
			}
			
			// send response
			if (getConn().writeObject(addFriendResponse)) {
				// response send 
			}
		}
		
		getConn().closeConnection();
	}

}
