package com.slm.slm1.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UserConnection {
	
	private Socket socket;
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;
	private ConcurrentLinkedQueue<TaskSerializable> tasksRecevied;
	
	public UserConnection(Socket socket) {
		this.socket = socket;
		
		try {
			OutputStream outputStream = socket.getOutputStream();
			objectOutputStream = new ObjectOutputStream(outputStream);
			
			InputStream inputStream = socket.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tasksRecevied = new ConcurrentLinkedQueue<>();
	}
	
	
	public Socket getSocket() {
		return socket;
	}

	public ObjectOutputStream getObjectOutputStream() {
		return objectOutputStream;
	}

	public ObjectInputStream getObjectInputStream() {
		return objectInputStream;
	}
	
	public ConcurrentLinkedQueue<TaskSerializable> getQueue() {
		return tasksRecevied;
	}
	
    public boolean writeObject(Object o) {
        boolean status = true;
        try {
            objectOutputStream.writeObject(o);
        } catch (IOException e) {
            closeConnection();
            status = false;
        }
        return status;
    }

    public Object readObject() {
        Object output = null;
        try {
            output = objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            closeConnection();
        }
        return output;
    }

	public void closeConnection() {
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
