package com.slm.slm1.helper;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import javax.jws.soap.SOAPBinding.Use;

import com.slm.slm1.daoImpl.LocationDao;
import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.ReceivedTask;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetail;
import com.slm.slm1.model.User;

public class OutgoingRunner extends ConnectionRunnerWithSync {


	public OutgoingRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {
		System.out.println("Outgoing runner");
		
		try {
			PollRequest pollRequest = (PollRequest) getConn().readObject();
			
			if (pollRequest != null) {
				// validate user
				UserDao userDao = ServerDatabase.getInstance().getUserDao();
				User user = userDao.get(pollRequest.senderUsername);
				
				// refresh entity manager
				DatabaseConnection databaseConnection = ServerDatabase.getInstance().getDatabaseConnection();
				databaseConnection.getEntityManager().refresh(user);
				user = userDao.get(pollRequest.senderUsername);
				
				if (user != null) {
					List<TaskSerializable> response = new ArrayList<TaskSerializable>();
					
					if (user.getReceivedTasks() != null && !user.getReceivedTasks().isEmpty()) {
						TaskDao taskDao = ServerDatabase.getInstance().getTaskDao();
						LocationDao locationDao = ServerDatabase.getInstance().getLocationDao();
						
						List<ReceivedTask> tasksToSend = user.getReceivedTasks();
						
						for (ReceivedTask received : tasksToSend) {
							Task task = received.getTask();
							
							TaskDetail taskDetail = task.getTaskDetail();
							if (taskDetail == null) {
								taskDetail = taskDao.get(task);
							}
							
							Location location = task.getLocation();
							
							TaskSerializable taskSerializable = new TaskSerializable();
							taskSerializable.taskName = task.getName();
							taskSerializable.taskNotes = taskDetail.getNotes();
							taskSerializable.completionStatus = task.getCompletionStatus() == 1 ? true : false;
							taskSerializable.locationLat = location.getLatitude();
							taskSerializable.locationLg = location.getLongitude();
							taskSerializable.deadline = task.getDeadline();
							taskSerializable.targetUsername = user.getUsername();
							taskSerializable.localTaskId = task.getId();
							
							response.add(taskSerializable);
							taskDao.delete(received);
						}
						
						user.setReceivedTasks(new ArrayList<ReceivedTask>());
						userDao.update(user);
					}
					
					if (getConn().writeObject(response)) {
						// response send
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			getConn().closeConnection();
			lock.unlock();
		}
	}

}
