package com.slm.slm1.helper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerRunner implements Runnable {

	protected final int portNumber;
	protected final IConnectionRunnerCreator connCreator; 
	protected boolean running;
		
	public ServerRunner(int portNumber, IConnectionRunnerCreator connCreator) {
		super();
		this.portNumber = portNumber;
		this.connCreator = connCreator;
	}

	@Override
	public void run() {
		this.running = true;
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e1) {
			this.running = false;
			e1.printStackTrace();
		}
		
		while(running) {
			try {
				Socket socket = serverSocket.accept();
				
				ConnectionRunner connectionRunner = connCreator.newObject(socket);
				new Thread(connectionRunner).run();
			} catch (IOException e) {
				this.running = false;
				e.printStackTrace();
			}
			
		}
		
	}
}
