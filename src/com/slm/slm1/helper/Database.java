package com.slm.slm1.helper;

public class Database {
	
	private String persistenceName;

	public Database(String persistenceName) {
		super();
		this.persistenceName = persistenceName;
	}

	public String getPersistenceName() {
		return persistenceName;
	}

	public void setPersistenceName(String persistenceName) {
		this.persistenceName = persistenceName;
	}
}
