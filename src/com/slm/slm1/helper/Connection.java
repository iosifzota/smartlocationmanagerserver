package com.slm.slm1.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;


public class Connection {

    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;

    public Connection(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean writeObject(Object o) {
        boolean status = true;
        try {
            getObjectOutputStream().writeObject(o);
        } catch (IOException e) {
            e.printStackTrace();
            closeConnection();
            status = false;
        }
        return status;
    }

    public Object readObject() {
        Object output = null;
        try {
            output = getObjectInputStream().readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            closeConnection();
        }
        return output;
    }

    public ObjectOutputStream getObjectOutputStream() {
        if (objectOutputStream == null) {
            try {
                OutputStream outputStream = socket.getOutputStream();
                objectOutputStream = new ObjectOutputStream(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
                closeConnection();
            }
        }
        return objectOutputStream;
    }

    public ObjectInputStream getObjectInputStream() {
        if (objectInputStream == null) {
            try {
                InputStream inputStream = socket.getInputStream();
                objectInputStream = new ObjectInputStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
                closeConnection();
            }
        }
        return objectInputStream;
    }

    public void closeConnection() {
        try {
        	System.out.println("Closing " + socket.getPort() + " port");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
