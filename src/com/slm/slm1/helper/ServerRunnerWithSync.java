package com.slm.slm1.helper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ServerRunnerWithSync implements Runnable {
	
	protected final int portNumber;
	protected final IConnectionRunnerWithSyncCreator connCreator; 
	protected boolean running;
	private Lock lock;

	public ServerRunnerWithSync(int portNumber, IConnectionRunnerWithSyncCreator connCreator, Lock lock) {
		this.portNumber = portNumber;
		this.connCreator = connCreator;
		this.lock = lock;
	}

	@Override
	public void run() {
		this.running = true;
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e1) {
			this.running = false;
			e1.printStackTrace();
		}
		
		while(running) {
			try {
				Socket socket = serverSocket.accept();
				
				lock.lock();
				ConnectionRunner connectionRunner = connCreator.newObject(socket, lock);
				new Thread(connectionRunner).run();
			} catch (IOException e) {
				this.running = false;
				e.printStackTrace();
			}
			
		}
	}
}
