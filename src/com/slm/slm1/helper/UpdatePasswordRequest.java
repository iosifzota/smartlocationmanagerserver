package com.slm.slm1.helper;

import java.io.Serializable;

public class UpdatePasswordRequest implements Serializable {
    public UserCredentials userCredentials;
    public String newPassword;
}
