package com.slm.slm1.helper;

import java.net.Socket;

public abstract class ConnectionRunner implements Runnable {
	
	private final Connection connection;

	public ConnectionRunner(Socket socket) {
		super();
		connection = new Connection(socket);
	}

	public Connection getConn() {
		return connection;
	}
}
