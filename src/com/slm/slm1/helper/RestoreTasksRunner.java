package com.slm.slm1.helper;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetail;
import com.slm.slm1.model.User;

public class RestoreTasksRunner extends ConnectionRunnerWithSync {

	public RestoreTasksRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {

		RestoreRequest restoreRequest = (RestoreRequest) getConn().readObject();
		
		if (restoreRequest != null) {
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
			User user = userDao.get(restoreRequest.username);
			
			if (user != null) {
				TaskDao taskDao = ServerDatabase.getInstance().getTaskDao();
				List<TaskSerializable> response = new ArrayList<>();
				
				for (Task task : user.getTasks()) {
					Location location = task.getLocation();
					TaskDetail taskDetail = task.getTaskDetail();
					
					if (taskDetail == null) {
						taskDetail = taskDao.get(task);
					}
					
					TaskSerializable taskSerializable = new TaskSerializable();
					taskSerializable.taskName = task.getName();
					taskSerializable.taskNotes = taskDetail.getNotes();
					taskSerializable.completionStatus = task.getCompletionStatus() == 1 ? true : false;
					taskSerializable.locationLat = location.getLatitude();
					taskSerializable.locationLg = location.getLongitude();
					taskSerializable.targetUsername = user.getUsername();
					taskSerializable.deadline = task.getDeadline();
					taskSerializable.localTaskId = task.getId();
					response.add(taskSerializable);
				}
				
				if (getConn().writeObject(response)) {
					// wrote
				}
			}
		}
		
		lock.unlock();
	}

}
