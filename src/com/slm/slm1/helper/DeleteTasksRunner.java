package com.slm.slm1.helper;

import java.net.Socket;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.model.Task;

public class DeleteTasksRunner extends ConnectionRunnerWithSync {

	public DeleteTasksRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {
		
		List<TaskSerializable> tasksToDelete = (List<TaskSerializable>) getConn().readObject();
		
		if (tasksToDelete != null && !tasksToDelete.isEmpty()) {
			TaskDao taskDao = ServerDatabase.getInstance().getTaskDao();
			Boolean success = false;
			
			for (TaskSerializable taskSerializable : tasksToDelete) {
				Task task = taskDao.get(taskSerializable.localTaskId);
				taskDao.delete(task);
			}
			
			success = true;
			if (getConn().writeObject(success)) {
				
			}
		}
		
		getConn().closeConnection();
		lock.unlock();
	}

}
