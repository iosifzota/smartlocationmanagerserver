package com.slm.slm1.helper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabaseConnection {
	private Database database;
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;

	public DatabaseConnection(Database database) {
		super();
		this.database = database;
		
		entityManagerFactory = Persistence.createEntityManagerFactory(database.getPersistenceName());
		entityManager = entityManagerFactory.createEntityManager();
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
