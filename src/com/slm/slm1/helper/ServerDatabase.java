package com.slm.slm1.helper;

import com.slm.slm1.daoImpl.LocationDao;
import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.daoImpl.UserDao;

public class ServerDatabase {
	private String persistenceName = "SmartLocationManagerServer";
	
	private DatabaseConnection databaseConnection;
	
	private static ServerDatabase instance; 
	
	private static UserDao userDao;
	private static LocationDao locationDao;
	private static TaskDao taskDao;
	
	private ServerDatabase() {
		Database database = new Database(persistenceName);
		this.databaseConnection = new DatabaseConnection(database);
		
		// last always
		userDao = new UserDao(this);
		locationDao = new LocationDao(this);
		taskDao = new TaskDao(this);
	}
	
	public static ServerDatabase getInstance() {
		if (instance == null) {
			instance = new ServerDatabase();
		}
		return instance;
	}

	public UserDao getUserDao() {
		return userDao;
	}
	
	public LocationDao getLocationDao() {
		return locationDao;
	}

	public TaskDao getTaskDao() {
		return taskDao;
	}

	public DatabaseConnection getDatabaseConnection() {
		return databaseConnection;
	}
}
