package com.slm.slm1.helper;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.slm.slm1.model.Task;

public class IncomingOutgoingManager {
	
	private static IncomingOutgoingManager instance;
	private ConcurrentLinkedQueue<Task> tasks;
	
	public static IncomingOutgoingManager getInstance() {
		if (instance == null) {
			instance = new IncomingOutgoingManager();
		}
		
		return instance;
	}
}
