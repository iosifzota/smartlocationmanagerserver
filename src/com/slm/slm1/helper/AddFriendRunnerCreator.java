package com.slm.slm1.helper;

import java.net.Socket;

public class AddFriendRunnerCreator implements IConnectionRunnerCreator {

	@Override
	public ConnectionRunner newObject(Socket socket) {
		return new AddFriendRunner(socket);
	}
}
