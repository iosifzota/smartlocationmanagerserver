package com.slm.slm1.helper;

import java.net.Socket;
import java.util.concurrent.locks.Lock;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.User;

public class UpdatePasswordRunner extends ConnectionRunnerWithSync {

	public UpdatePasswordRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {

		UpdatePasswordRequest updatePasswordRequest = (UpdatePasswordRequest) getConn().readObject();
		
		if (updatePasswordRequest != null) {
			// Create response
			UpdatePasswordResponse updatePasswordResponse = new UpdatePasswordResponse();
			updatePasswordResponse.status = false;
			
			// check if user is already registered
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
	        User user = userDao.get(updatePasswordRequest.userCredentials.username);
	        
	        // Get appropriate user, if exits
	        if (user != null) {	        	
	        	// Update password
	        	user.setPassword(updatePasswordRequest.newPassword);
	        	userDao.update(user);
	        	
	        	updatePasswordResponse.status = true;
	        }

			if (getConn().writeObject(updatePasswordResponse)) {
				// responded
			}
		}
		
		getConn().closeConnection();
		lock.unlock();
	}
}
