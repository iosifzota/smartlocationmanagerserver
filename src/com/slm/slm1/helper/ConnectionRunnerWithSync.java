package com.slm.slm1.helper;

import java.net.Socket;
import java.util.concurrent.locks.Lock;

public abstract class ConnectionRunnerWithSync extends ConnectionRunner {
	
	protected final Lock lock;

	public ConnectionRunnerWithSync(Socket socket, Lock lock) {
		super(socket);
		this.lock = lock;
	}
}
