package com.slm.slm1.helper;

import java.net.Socket;
import java.util.concurrent.locks.Lock;

public class UpdatePasswordRunnerCreator implements IConnectionRunnerWithSyncCreator {

	@Override
	public ConnectionRunnerWithSync newObject(Socket socket, Lock lock) {
		return new UpdatePasswordRunner(socket, lock);
	}

}
