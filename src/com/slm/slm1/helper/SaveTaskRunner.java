package com.slm.slm1.helper;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.mysql.fabric.Server;
import com.slm.slm1.daoImpl.LocationDao;
import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetail;
import com.slm.slm1.model.User;

public class SaveTaskRunner extends ConnectionRunnerWithSync {

	public SaveTaskRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {
		
		List<TaskSerializable> tasksToSave = (List<TaskSerializable>) getConn().readObject();
		
		if (tasksToSave != null && !tasksToSave.isEmpty()) {
			String username = tasksToSave.get(0).targetUsername;
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
			User user = userDao.get(username);
			
			if (user != null) {
				List<Integer> savedTasksId = new ArrayList<>();
				
				for (TaskSerializable taskSerializable : tasksToSave) {
				
					LocationDao locationDao = ServerDatabase.getInstance().getLocationDao();
					Location location = locationDao.get(taskSerializable.locationLat, taskSerializable.locationLg);
					
					if (location == null) {
						Location tmpLocation = new Location();
						tmpLocation.setLatitude(taskSerializable.locationLat);
						tmpLocation.setLongitude(taskSerializable.locationLg);
						
						locationDao.insert(tmpLocation);
						location = locationDao.get(taskSerializable.locationLat, taskSerializable.locationLg);
					}
					
					TaskDao taskDao = ServerDatabase.getInstance().getTaskDao();
					
					Task task = new Task();
					task.setName(taskSerializable.taskName);
					task.setCompletionStatus(taskSerializable.completionStatus ? 1 : 0);
					task.setDeadline(taskSerializable.deadline);
					task.setUser(user);
					task.setLocation(location);
					taskDao.insert(task);
					
					TaskDetail taskDetail = new TaskDetail();
					taskDetail.setNotes(taskSerializable.taskNotes);
					taskDetail.setTask(task);
					taskDao.insert(taskDetail);
					
					savedTasksId.add(task.getId());
					System.out.println("Saved task() - " + task.getName());
				}
				
				if (getConn().writeObject(savedTasksId)) {
					// responseSend
				}
			}
		}

		getConn().closeConnection();
		lock.unlock();
	}

}
