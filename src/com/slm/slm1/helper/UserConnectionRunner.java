package com.slm.slm1.helper;

import java.io.IOException;
import java.util.List;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.main.Server;
import com.slm.slm1.model.User;

public class UserConnectionRunner implements Runnable {
	
	private UserConnection userConnection;
	private boolean connectionIsActive;
	private boolean loginDone;
	
	public UserConnectionRunner(UserConnection userConnection) {
		super();
		this.userConnection = userConnection;
		this.loginDone = false;
	}

	@Override
	public void run() {
        connectionIsActive = true;
        while (connectionIsActive) {
        	if (!loginDone) {
        		handleLogin();
        		continue;
        	}
        	
            handleTaskCommunication();
        }
	}
	
	private void handleTaskCommunication() {
		TaskSerializable receivedTask;
		try {
			Object request = userConnection.getObjectInputStream().readObject();
			
			if (request == null) {
				throw new RuntimeException("Failed to read stream");
			}
			
			PollRequest pollRequest = null;
			try {				
				pollRequest = (PollRequest) request;
			} catch (ClassCastException e) {
				
			}
			
			if (pollRequest != null) {
				handlePollRequest(pollRequest);
				return;
			}
			
			SendRequest sendRequest = (SendRequest) request;
			
			if (sendRequest != null) {
				handleSendRequest(sendRequest);
				return;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			closeConnection();
		}
	}

	private void handleSendRequest(SendRequest sendRequest) {
		int sendCount = 0;
		
		System.out.println("About to receive: " + sendRequest.nbTasksToSend + " tasks");
		while (sendCount < sendRequest.nbTasksToSend) {
			TaskSerializable taskSerializable;
			try {
				taskSerializable = (TaskSerializable) (userConnection.getObjectInputStream().readObject());
				
				if (taskSerializable == null) {
                    throw new RuntimeException("Serializable task is null");
                }
				
				UserConnection targetUserConnection = UsersRouter.getInstance().getUserConnections().get(taskSerializable.targetUsername);

				targetUserConnection.getQueue().add(taskSerializable);
				
				System.out.println("Received " + (sendCount + 1) + " tasks");
				sendCount++;
				
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	private void handleLogin() {
        LoginRequest loginRequest = (LoginRequest) (userConnection.readObject());
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.status = false;
        
//        if (loginRequest != null) {
//	        LoginRunner loginRunner = new LoginRunner(userConnection, loginRequest.userCredentials);
//	        loginRunner.run();
//	        
//	        User user = loginRunner.getUser();
//	        
//	        if (user != null) {
//	        	loginDone = true;
//	        	loginResponse.status = true;
//	        	UsersRouter.getInstance().getUserConnections().put(user.getUsername(), userConnection);
//	        }
//        }
//        
//        userConnection.writeObject(loginResponse);
	}

	public void handlePollRequest(PollRequest pollRequest) {
		
		System.out.println("Received poll request");
		
		if (!userConnection.getQueue().isEmpty()) {
			TaskSerializable[] tasksToSend = userConnection.getQueue().toArray(new TaskSerializable[0]);
		
			PollRequestResponse pollRequestResponse = new PollRequestResponse();
			pollRequestResponse.nbTasksToReceive = tasksToSend.length;
			
			try {
				userConnection.getObjectOutputStream().writeObject(pollRequestResponse);
				
				for (TaskSerializable taskSerializable : tasksToSend) {
					userConnection.getObjectOutputStream().writeObject(taskSerializable);
				}
				userConnection.getQueue().clear();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				closeConnection();
			}
		} else {
			PollRequestResponse pollRequestResponse = new PollRequestResponse();
			pollRequestResponse.nbTasksToReceive = 0;
			
			try {
				userConnection.getObjectOutputStream().writeObject(pollRequestResponse);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				closeConnection();
			}
		}
	}
	
	public void closeConnection() {
		System.out.println("Closing connection to client.");
		connectionIsActive = false;
		userConnection.closeConnection();
	}

}
