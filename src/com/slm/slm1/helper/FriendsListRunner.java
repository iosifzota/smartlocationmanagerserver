package com.slm.slm1.helper;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.User;


public class FriendsListRunner extends ConnectionRunner {

	public FriendsListRunner(Socket socket) {
		super(socket);
	}

	@Override
	public void run() {

		FriendsListRequest friendsListRequest = (FriendsListRequest) getConn().readObject();
		
		if (friendsListRequest != null) {
			List<FriendSerializable> response = new ArrayList<FriendSerializable>();
			
			// validate user
			UserDao userDao = ServerDatabase.getInstance().getUserDao();
			User user = userDao.get(friendsListRequest.senderUsername);
			
			if (user != null) {
				List<User> friends = userDao.getFriends(user);
				
				if (friends != null) {
					for (User friendUser : friends) {
						FriendSerializable friendSerializable = new FriendSerializable();
						friendSerializable.username = friendUser.getUsername();
						
						response.add(friendSerializable);
					}
				}
			}
			
			if (getConn().writeObject(response)) {
				// response send
			}
			
		}
		
		getConn().closeConnection();
	}

}
