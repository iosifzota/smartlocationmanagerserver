package com.slm.slm1.helper;

import java.net.Socket;
import java.util.concurrent.locks.Lock;

public class RestoreTasksRunnerCreator implements IConnectionRunnerWithSyncCreator {

	@Override
	public ConnectionRunnerWithSync newObject(Socket socket, Lock lock) {
		return new RestoreTasksRunner(socket, lock);
	}

}
