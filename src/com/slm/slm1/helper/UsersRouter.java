package com.slm.slm1.helper;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class UsersRouter {
	
	private static UsersRouter instance;
	private ConcurrentMap<String, UserConnection> userConnections;
	
	private UsersRouter() {
		userConnections = new ConcurrentHashMap<>();
	}
	
	public static UsersRouter getInstance() {
		if (instance == null) {
			instance = new UsersRouter();
		}
		return instance;
	}

	public ConcurrentMap<String, UserConnection> getUserConnections() {
		return userConnections;
	}
}
