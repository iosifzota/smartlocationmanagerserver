package com.slm.slm1.helper;

import java.net.Socket;

public class FriendsListRunnerCreator implements IConnectionRunnerCreator {

	@Override
	public ConnectionRunner newObject(Socket socket) {
		return new FriendsListRunner(socket);
	}

}
