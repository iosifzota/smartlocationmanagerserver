package com.slm.slm1.helper;

import java.net.Socket;

public class AuthRunnerCreator implements IConnectionRunnerCreator {

	@Override
	public ConnectionRunner newObject(Socket socket) {
		return new AuthRunner(socket);
	}

}
