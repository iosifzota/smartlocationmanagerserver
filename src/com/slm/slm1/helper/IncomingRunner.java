package com.slm.slm1.helper;

import java.net.Socket;
import java.util.concurrent.locks.Lock;

import com.slm.slm1.daoImpl.LocationDao;
import com.slm.slm1.daoImpl.TaskDao;
import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.ReceivedTask;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetail;
import com.slm.slm1.model.User;

public class IncomingRunner extends ConnectionRunnerWithSync {

	public IncomingRunner(Socket socket, Lock lock) {
		super(socket, lock);
	}

	@Override
	public void run() {
		System.out.println("Incomming runner");
		try {
			TaskSerializable taskSerializable = (TaskSerializable) getConn().readObject();
			

			
			if (taskSerializable != null) {
				System.out.println("received task - " + taskSerializable.taskName);
				
				UserDao userDao = ServerDatabase.getInstance().getUserDao();
				User user = userDao.get(taskSerializable.targetUsername);
				TaskDao taskDao = ServerDatabase.getInstance().getTaskDao();
				Boolean success = false;
				
				// if it is a new task
				if (user != null && taskDao.get(taskSerializable.localTaskId) != null) {
					// task is already in the database
					// it's a forward
					Task task = taskDao.get(taskSerializable.localTaskId);
					task.setUser(user);
					taskDao.update(task);
					
					ReceivedTask receivedTask = new ReceivedTask();
					receivedTask.setTask(task);
					receivedTask.setUser(user);
					taskDao.insert(receivedTask);
					
					success = true;
				}
				
				if (getConn().writeObject(success)) {
					// responded
				}
			}
		} finally {
			getConn().closeConnection();
			lock.unlock();
		}
	}

}
