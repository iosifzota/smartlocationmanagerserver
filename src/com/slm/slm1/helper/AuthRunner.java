package com.slm.slm1.helper;

import java.net.Socket;

import com.slm.slm1.daoImpl.UserDao;
import com.slm.slm1.model.User;

public class AuthRunner extends ConnectionRunner {

	public AuthRunner(Socket socket) {
		super(socket);
	}

	@Override
	public void run() {
		System.out.println("Auth runner");
		
		LoginRequest loginRequest = (LoginRequest) getConn().readObject();
        
        if (loginRequest != null) {
        	LoginResponse loginResponse = new LoginResponse();
            loginResponse.status = false;

            UserDao userDao = ServerDatabase.getInstance().getUserDao();
	        User user = userDao.get(loginRequest.userCredentials.username);
	        
	        if (user != null && user.getPassword().equals(loginRequest.userCredentials.password)) {
	        	loginResponse.status = true;
	        }
	        
	        getConn().writeObject(loginResponse);
        }
        
        getConn().closeConnection();
	}

}
