package com.slm.slm1.helper;

import java.net.Socket;
import java.util.concurrent.locks.Lock;

public interface IConnectionRunnerWithSyncCreator {

	ConnectionRunnerWithSync newObject(Socket socket, Lock lock);
}
