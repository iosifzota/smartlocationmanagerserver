package com.slm.slm1.dao;

public interface IDao<Entity> {

	void insert(Entity entity);
	Entity get(int id);
	void update(Entity entity);
	void delete(Entity entity);
}
